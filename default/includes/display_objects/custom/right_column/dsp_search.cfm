<div id="blog-search">
	<cfoutput>
		<form action="#$.createHREF(filename='search')#" id="searchForm">
			<input type="hidden" name="display" value="search"/>
			<input type="hidden" name="newSearch" value="true"/>
			<input type="hidden" name="noCache" value="1"/>
			<h3>Search</h3>
			<input type="text" name="keywords" class="required inline-block" placeholder="Enter Keyword" />
			<button name="submit" type="submit" class="icon inline-block">&##xe643;</button>
		</form>
	</cfoutput>
</div><!--- #/blog-search --->