<cfsilent>
	<cfset contentID = "2ADD8BE5-97C8-4C35-98816DFFF81392CE" />
	<cfset archive = $.getBean('content').loadBy(contentid=contentID)>
	<cfset rsArchive = $.getBean('contentGateway').getReleaseCountByMonth($.event('siteID'),contentID)>
	<cfset hasArchiveFilter = listFindNoCase("releaseMonth,releaseDate,releaseYear",$.event("filterBy"))>
</cfsilent>

<div id="archives-container">
	<h3>Archives</h3>
	<cfoutput query="rsArchive" group="year">
		<cfset isCurrentArchive = hasArchiveFilter and $.event("month") eq rsArchive.month and $.event("year") eq rsArchive.year>
		<cfoutput>
				<a href="#$.createHREF(filename='#archive.getFilename()#/date/#rsArchive.year#/#rsArchive.month#/')#?year=#rsArchive.year#&month=#rsArchive.month#">
					<i class="icon blue icon-chevron-right inline-block">&##xe633;</i>
					#monthasstring(rsArchive.month)# #year# - <span>(#rsArchive.items# posts)</span>
				</a>
		</cfoutput>
	</cfoutput>
</div>
