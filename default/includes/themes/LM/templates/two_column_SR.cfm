<cfoutput>
	<cfinclude template="inc/html_head.cfm" />
	<body id="#$.getTopID()#" class="#$.createCSSid($.content('menuTitle'))#">

		<!--- Header --->
			<cfinclude template="inc/body_head.cfm" />

		<!--- Main --->
			<div class="main container #$.getMBContainerClass()# clearfix">
				<cfinclude template="inc/blog_body_head.cfm" />
				<!--- Breadcrumbs --->
	                <cfinclude template="inc/breadcrumb.cfm" />

				<!--- Blog --->
					<section id="left-column" class="section pull-left">
	                	#$.dspBody(body=$.content('body'),pageTitle='',crumbList=0,showMetaImage=1)#
	               	</section>
	               	<aside id="right-column" class="pull-right">
						#$.dspObjects(3)#
	               	</aside>

			</div><!-- /.main -->
			<footer></footer>

	<cfinclude template="inc/html_foot.cfm" />
</cfoutput>