<cfoutput>
    <a href="/" id="blog-link"><span class="icon bg-blue-light">&##xe609;</span><span class="label bg-blue-light">PROFILE</span></a>
    <!-- Profile -->
        <section id="profile" class="section">
            <div class="section-info clearfix">
                <div class="user pull-left">
                    <a href="/blog" class="profile-pic inline-block small">
                        <img src="#$.siteConfig('themeAssetPath')#/images/lmmg.jpg" alt="Luis Matute">
                    </a>
                    <h1 class="inline-block">Luis Matute <span class="blue">Web Dev</span></h1>
                </div>
            </div>
        </section>
</cfoutput>