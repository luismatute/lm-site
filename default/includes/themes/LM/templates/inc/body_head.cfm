<!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Colored Bar at the top -->
        <ul class="colored clearfix">
            <li class="bg-blue-light"></li>
            <li class="bg-blue"></li>
            <li class="bg-green"></li>
            <li class="bg-yellow"></li>
            <li class="bg-red"></li>
        </ul>

<!-- Social Media -->
    <div class="container clearfix">
        <ul id="social-media" class="clearfix">
            <li><a href="https://www.facebook.com/lmatute10" class="icon" target="_blank">&#xe61b;</a></li>
            <li><a href="https://twitter.com/luis_matute" class="icon" target="_blank">&#xe61a;</a></li>
            <li><a href="http://www.linkedin.com/pub/luis-matute/50/458/82b/" class="icon" target="_blank">&#xe624;</a></li>
            <li><a href="https://bitbucket.org/luismatute" class="icon" target="_blank">&#xe63c;</a></li>
        </ul>
    </div>