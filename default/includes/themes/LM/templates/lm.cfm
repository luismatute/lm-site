<cfoutput>
	<cfinclude template="inc/html_head.cfm" />
	<body id="#$.getTopID()#" class="#$.createCSSid($.content('menuTitle'))#">

		<!--- Header --->
			<cfinclude template="inc/body_head.cfm" />

        <!-- CV -->
            <div class="main container #$.getMBContainerClass()#">
                <!--- <a href="/blog/" id="blog-link"><span class="icon bg-blue-light">&##xe60b;</span><span class="label bg-blue-light">BLOG</span></a> --->
                <!-- Profile -->
                    <section id="profile" class="section">
                        <header class="active">
                            <a href="" class="tab show">
                                <span class="icon bg-blue">&##xe609;</span>
                                <div class="divider"></div>
                                <span class="section-name">PROFILE</span>
                            </a>
                            <div class="chevron-down tab"><i class="icon">&##xe62c;</i></div>
                        </header>
                        <div class="section-info clearfix">
                            <div class="user pull-left">
                                <div class="profile-pic inline-block">
                                    <img src="#$.siteConfig('themeAssetPath')#/images/lmmg.jpg" alt="Luis Matute">
                                </div>
                                <h1 class="inline-block">Luis Matute <span class="blue">Web Dev</span></h1>
                            </div>
                            <ul class="user-info pull-right">
                                <li><span>Name:</span> Luis Matute</li>
                                <li><span>DOB:</span> June 18, 1988</li>
                                <li><span>Phone:</span> <a href="tel:+50497078677">+504.9707.8677</a></li>
                                <li><span>Email:</span> <a href="mailto:luis.matute@me.com">luis.matute@me.com</a></li>
                                <li><span>Website:</span> <a href="/blog">http://www.luismatute.me/blog</a></li>
                            </ul>
                        </div>
                        <div class="section-body">
                            <p>I have always enjoyed thinking creatively about problems, designing solutions, and then building products that meet those goals. In the process you end up learning a lot about yourself, your team members, limitations, and possibilities - not to mention the satisfaction of playing an active part in the transformation from idea to reality.</p>
                        </div>
                    </section>
                <!-- Resume -->
                    <section id="resume" class="section">
                        <header>
                            <a href="" class="tab">
                                <span class="icon bg-green">&##xe61f;</span>
                                <div class="divider"></div>
                                <span class="section-name">RESUME</span>
                            </a>
                            <div class="chevron-down tab"><i class="icon">&##xe62c;</i></div>
                        </header>
                        <div class="section-body clearfix">
                            <a href="#$.siteConfig('themeAssetPath')#/assets/files/LM-resume.pdf" id="print-cv" target="_blank"><span class="icon bg-green">&##xe60d;</span><span class="label bg-green">PRINT</span></a>
                            <!-- Skills -->
                                <div id="skills" class="pull-right right-col">
                                    <h3>PROGRAMING SKILLS</h3>
                                    <div class="programming">
                                        <h4>HTML</h4>
                                        <div class="skill-bar">
                                            <div class="bar bg-blue-light" style="width: 90%">
                                                <p>90%</p>
                                            </div>
                                        </div>
                                        <h4>CSS</h4>
                                        <div class="skill-bar">
                                            <div class="bar bg-blue" style="width: 90%">
                                                <p>90%</p>
                                            </div>
                                        </div>
                                        <h4>Javascript/jQuery</h4>
                                        <div class="skill-bar">
                                            <div class="bar bg-green" style="width: 85%">
                                                <p>85%</p>
                                            </div>
                                        </div>
                                        <h4>Coldfusion</h4>
                                        <div class="skill-bar">
                                            <div class="bar bg-yellow" style="width: 70%">
                                                <p>70%</p>
                                            </div>
                                        </div>
                                        <h4>NodeJS</h4>
                                        <div class="skill-bar">
                                            <div class="bar bg-orange" style="width: 15%">
                                                <p>15%</p>
                                            </div>
                                        </div>
                                        <h4>SQL/NoSQL</h4>
                                        <div class="skill-bar">
                                            <div class="bar bg-red" style="width: 60%">
                                                <p>60%</p>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>LANGUAGE SKILLS</h3>
                                    <div class="language">
                                        <div class="row">
                                            <h5 class="inline-block">Espa&ntilde;ol</h5>
                                            <div class="inline-block circles">
                                                <div class="bg-blue-light inline-block"></div>
                                                <div class="bg-blue-light inline-block"></div>
                                                <div class="bg-blue-light inline-block"></div>
                                                <div class="bg-blue-light inline-block"></div>
                                                <div class="bg-blue-light inline-block"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <h5 class="inline-block">English</h5>
                                            <div class="inline-block">
                                                <div class="inline-block circles">
                                                    <div class="bg-green inline-block"></div>
                                                    <div class="bg-green inline-block"></div>
                                                    <div class="bg-green inline-block"></div>
                                                    <div class="bg-green inline-block"></div>
                                                    <div class="empty inline-block"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <h5 class="inline-block">Fran&ccedil;ais</h5>
                                            <div class="inline-block">
                                                <div class="inline-block circles">
                                                    <div class="bg-yellow inline-block"></div>
                                                    <div class="empty inline-block"></div>
                                                    <div class="empty inline-block"></div>
                                                    <div class="empty inline-block"></div>
                                                    <div class="empty inline-block"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- Resume -->
                                <div id="my-story" class="left-col">
                                    <section class="employment">
                                        <h3><i class="icon bg-blue">&##xe617;</i>EMPLOYMENT</h3>
                                        <article class="first">
                                            <header>
                                                <h4>
                                                    <i class="icon blue icon-chevron-right inline-block">&##xe633;</i> 
                                                    Web Developer
                                                </h4>
                                                <h6>2013-Present</h6>
                                                <h5>Fluky Factory</h5>
                                            </header>
                                            <p>Co-founder and partner at Fluky Factory - professional firm that creates web solutions to make businesses worldwide soar</p>
                                        </article>
                                        <article>
                                            <header>
                                                <h4>
                                                    <i class="icon blue icon-chevron-right inline-block">&##xe633;</i> 
                                                    Front End Web Developer
                                                </h4>
                                                <h6>2011-Present</h6>
                                                <h5>Sandals Resorts International (San Services)</h5>
                                            </header>
                                            <p>As a Front End Developer, the main responsibility is for the production, modification and maintenance of websites and web application's user interfaces. This involves working closely with designers, in using semantic mark-up language i.e. HTML/CSS, to turn their static Photoshop designs into working browser based web pages. Also work closely with server-side developers to implement their server-side code in order to develop complex, interactive and database driven websites.</p>
                                        </article>
                                        <article>
                                            <header>
                                                <h4>
                                                    <i class="icon blue icon-chevron-right inline-block">&##xe633;</i> 
                                                    Web Dev Free Lance
                                                </h4>
                                                <h6>2011-Present</h6>
                                            </header>
                                            <p>Individual projects presented to independent employers at different points.</p>
                                        </article>
                                        <article>
                                            <header>
                                                <h4>
                                                    <i class="icon blue icon-chevron-right inline-block">&##xe633;</i> 
                                                    American Airlines
                                                </h4>
                                                <h6>2006-2011</h6>
                                                <h5>Lead Security Agent</h5>
                                            </header>
                                            <p>Implementing TSA Security Directives and International Security Program procedures. Ensuring screening and searches of all passengers, baggage, cargo and aircraft cabin is conducted in compliance with the measures set forth in AOSSP, ISP and TSA Security Directives. Training and supervising contracted security personnel who perform aircraft cabin search in the aircraft.</p>
                                        </article>
                                    </section>
                                    <section class="education">
                                        <h3><i class="icon bg-green">&##xe617;</i>EDUCATION</h3>
                                        <article class="first">
                                            <header>
                                                <h4>
                                                    <i class="icon green icon-chevron-right inline-block">&##xe633;</i> 
                                                    Bachelor of Computer Science
                                                </h4>
                                                <h6>2005-2012</h6>
                                                <h5>Universidad Catolica de Honduras <br /><span class="show">San Pedro Sula, Cort&eacute;s, Honduras</span></h5>
                                            </header>
                                        </article>
                                        <article>
                                            <header>
                                                <h4>
                                                    <i class="icon green icon-chevron-right inline-block">&##xe633;</i> 
                                                    High School
                                                </h4>
                                                <h6>2005</h6>
                                                <h5>Liceo Bilingue Centroamericano <br/ > <span class="show">San Pedro Sula, Cort&eacute;s, Honduras</span></h5>
                                            </header>
                                        </article>
                                    </section>
                                </div>
                        </div>
                    </section>
                <!-- Portfolio -->
                    <section id="portfolio" class="section">
                        <header>
                            <a href="" class="tab">
                                <span class="icon bg-yellow">&##xe620;</span>
                                <div class="divider"></div>
                                <span class="section-name">PORTFOLIO</span>
                            </a>
                            <div class="chevron-down tab"><i class="icon">&##xe62c;</i></div>
                        </header>
                        <div class="section-body">
                            <ul id="portfolio-nav">
                                <li><a href="##" data-filter="*">All</a></li>
                                <li><a href="##" data-filter=".fluky">Fluky</a></li>
                                <li><a href="##" data-filter=".work">Work</a></li>
                            </ul>
                            <div id="my-projects">
                                <a href="http://www.flukyfactory.com/" rel="nofollow" target="_blank" class="fluky project"><img src="#$.siteConfig('themeAssetPath')#/images/projects/fluky.jpg" alt="Fluky Factory"></a>
                                <a href="http://www.catchadvisorygroup.com/" rel="nofollow" target="_blank" class="fluky project"><img src="#$.siteConfig('themeAssetPath')#/images/projects/catch.jpg" alt="Catch Advisory Group"></a>
                                <a href="http://www.sandals.com/" rel="nofollow" target="_blank" class="work project"><img src="#$.siteConfig('themeAssetPath')#/images/projects/sandals.jpg" alt="Sandals Resorts"></a>
                                <a href="http://www.beaches.com/" rel="nofollow" target="_blank" class="work project"><img src="#$.siteConfig('themeAssetPath')#/images/projects/beaches.jpg" alt="Beaches Resorts"></a>
                                <a href="http://www.sandalsweddingblog.com/" rel="nofollow" target="_blank" class="work project"><img src="#$.siteConfig('themeAssetPath')#/images/projects/sandalsweddingblog.jpg" alt="Sandals Wedding blog"></a>
                                <a href="http://www.amoroforsandals.com/" rel="nofollow" target="_blank" class="work project"><img src="#$.siteConfig('themeAssetPath')#/images/projects/amoro.jpg" alt="Amoro for Sandals"></a>
                            </div>
                        </div>
                    </section>
                <!-- Contact -->
                    <section id="contact" class="section">
                        <header>
                            <a href="" class="tab">
                                <span class="icon bg-red">&##xe625;</span>
                                <div class="divider"></div>
                                <span class="section-name">CONTACT</span>
                            </a>
                            <div class="chevron-down tab"><i class="icon">&##xe62c;</i></div>
                        </header>

                        <div class="section-body clearfix">
                            <!-- Map -->
                            <div id="gmap">
                                <div class="googlemaps"></div>
                                <div class="info">
                                    <p>CONTACT INFO</p>
                                    <ul>
                                        <li><a href="https://maps.google.com/maps?q=San+Pedro+Sula,+Cort%C3%A9s,+Honduras&hl=en&ie=UTF8&sll=37.0625,-95.677068&sspn=76.603666,175.869141&oq=san+pedro+sula&hnear=San+Pedro+Sula,+Cort%C3%A9s,+Honduras&t=m&z=12" rel="nofollow" target="_blank"><i class="inline-block icon">&##xe61d;</i> San Pedro Sula, Honduras</a></li>
                                        <li><a href="tel:+50497078677"><i class="inline-block icon">&##xe64c;</i>+504 9707 8677</a></li>
                                        <li><a href="mailto:luis.matute@me.com"><i class="inline-block icon">&##xe625;</i> luis.matute@me.com</a></li>
                                        <li><a href="http://www.luismatute.me"><i class="inline-block icon">&##xe621;</i> www.luismatute.me</a></li>
                                    </ul>
                                </div>
                            </div>

                            <!-- vcard -->
                            <div id="vcard" class="pull-right">
                                <h4>VCARD</h4>
                                <!--- <img src="" alt="QR link to vcard" width="160" height="160"> --->
                                <a href="#$.siteConfig('themeAssetPath')#/assets/files/Luis-Matute.vcf">
                                    <img src="#$.siteConfig('themeAssetPath')#/assets/files/qrcode.png" alt="QR link to vcard" width="160">
                                </a>
                            </div>
                            <!-- Contact Form -->
                            <div class="pull-left contact-form-container">
                                <h3><i class="icon bg-red">&##xe617;</i>LET'S KEEP IN TOUCH</h3>
                                <!--- <form action="POST">
                                    <div class="control-row pull-left">
                                        <label for="contact-name">Name <sup>*</sup></label>
                                        <input type="text" name="name" id="contact-name" class="required">
                                    </div>
                                    <div class="control-row pull-right clearfix">
                                        <label for="contact-email">Email <sup>*</sup></label>
                                        <input type="email" name="email" id="contact-email">
                                    </div>
                                    <label for="contact-message">Message <sup>*</sup></label>
                                    <textarea name="message" id="contact-message" rows="5" class="required"></textarea>
                                    <button type="submit">Submit</button>
                                </form> --->
                                #$.dspObjects(2)#
                            </div>
                        </div>
                    </section>
            </div> <!--- ./main --->
            <footer></footer>

	<cfinclude template="inc/html_foot.cfm" />
</cfoutput>