<cfoutput>
	<cfinclude template="inc/html_head.cfm" />
	<body id="#$.getTopID()#" class="#$.createCSSid($.content('menuTitle'))#">

		<!--- Header --->
			<cfinclude template="inc/body_head.cfm" />

		<!--- Main --->
			<div class="main container #$.getMBContainerClass()# clearfix">
				<cfinclude template="inc/blog_body_head.cfm" />
				<!--- Breadcrumbs --->
	                <cfinclude template="inc/breadcrumb.cfm" />

				<!--- Blog --->
					<section id="left-column" class="section pull-left post">
						<cfsilent>
							<cfset $post = $.getBean('content').loadBy(contentid=$.content().getValue('contentID')) />
							<cfset topID = $.getTopID() />
						</cfsilent>
						<!--- Post Header --->
							<header class="active post-header">
				                <div class="show">
				                    <span class="icon bg-blue-light">&##xe60b;</span>
				                    <div class="divider"></div>
				                    <span class="section-name post-name inline-block">#Ucase(LSDateFormat($post.getValue('releaseDate'),"MMM.DD"))#</span>

									<cfif len($post.getValue('lastUpdateBy'))>
										<p class="inline-block header-item">
											<i class="icon inline-block header-icon">&##xe609;</i>
				                    		<a href="/" class="inline-block">#$post.getValue('lastUpdateBy')#</a>
				                    	</p>
				                    </cfif>
				                    <cfif listLen($post.getValue('tags')) GT 0>
				                    	<cfset tagLen=listLen($post.getValue('tags')) />
				                    	<cfset counter = iif(tagLen GT 3, 3, tagLen) />
				                    	<p class="inline-block header-item">
					                    	<i class="inline-block icon">&##xe640;</i>
						                    <cfloop from="1" to="#counter#" index="t">
												<cfset tag=#trim(listgetAt($post.getValue('tags'),t))#>
												<a href="#$.createHREF(filename='#topID#/tag/#urlEncodedFormat(tag)#')#" class="inline-block">#HTMLEditFormat(tag)#</a>
												<cfif tagLen gt t>, </cfif>
											</cfloop>
										</p>
									</cfif>
				                </div>
				            </header>

						<!--- Post Content --->
							<div class="post-content">
								<!--- Post Body --->
									<div class="actual-post">
										#$.dspBody(body=$.content('body'),pageTitle=$.content('title'),crumbList=0,showMetaImage=1)#
									</div>

								<!--- Post Tags --->
									<div class="tags">
										<cfif listLen($.content().getValue('tags')) GT 0>
											<cfset tagLen=listLen($.content().getValue('tags')) />
		                    				<cfset counter = iif(tagLen GT 3, 3, tagLen) />
		                    				<h5 class="inline-block">Tags:</h5>
											<ul class="inline-block">
												<cfloop from="1" to="#counter#" index="t">
													<cfset tag=#trim(listgetAt($.content().getValue('tags'),t))#>
													<li class="btn-gen inline-block">
														<a href="/#topID#/tag/#urlEncodedFormat(tag)#">#HTMLEditFormat(tag)#</a>
													</li>
												</cfloop>
											</ul>
										</cfif>
									</div>
							</div> <!--- ./post-content --->

						<!--- Middle Column Objects --->
							#$.dspObjects(2)#

	               	</section>
	               	<aside id="right-column" class="pull-right">
						#$.dspObjects(3)#
	               	</aside>

			</div><!-- /.main -->
			<footer></footer>

	<cfinclude template="inc/html_foot.cfm" />
</cfoutput>